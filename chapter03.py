from bs4 import BeautifulSoup
from urllib.request import urlopen

url = "https://en.wikipedia.org/wiki/Kevin_Bacon"
html = urlopen(url)
bsObj = BeautifulSoup(html.read(), features="html.parser")


for link in bsObj.findAll("a"):
    if 'href' in link.attrs:
        print(link.attrs['href'])
