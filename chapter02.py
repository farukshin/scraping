from bs4 import BeautifulSoup
from urllib.request import urlopen

url = "https://www.pythonscraping.com/pages/warandpeace.html"
html = urlopen(url)
bsObj = BeautifulSoup(html.read(), features="html.parser")

nameList = bsObj.findAll("span", {"class": "green"})
for name in nameList:
    print(name.get_text())
